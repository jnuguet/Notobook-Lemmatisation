 Candida pax grata nobis uice temporis annum
Post hiemes actas tranquillo lumine ducit,
Signatam que diem sancti Felicis honore
Securis aperit populis; gaudere serenis
Mentibus abstersa diri caligine belli
Suadet ouans Felix, quia, pacis et ipse patronus
Cum patribus Paulo atque Petro et cum fratribus almis
Martyribus, Regem regum exorauit amico
Numine Romani producere tempora regni
Instantes que Getas ipsis iam faucibus Vrbis
Pellere, et exitium seu uincula uertere in ipsos
Qui minitabantur Romanis ultima regnis.
Nunc igitur pulsa formidine ut imbribus actis
Respicere expulsas nubes, praesentia rerum
Praeteritis conferre iuuat: quam taetra per istos
Qui fluxere dies elapso nox erat anno,
Cum furor accensus diuinae motibus irae
Immisso Latiis arderet in urbibus hoste!
Nunc itidem placidi spectata potentia Christi
Munera: mactatis pariter cum rege profano
Hostibus Augusti pueri, uictoria pacem
Reddidit, atque annis tener idem fortis in armis
Praeualuit uirtute Dei et mortalia fregit
Robora, sacrilegum Christo superante tyrannum.
Sed quid ego hinc modo plura loquar, quod non speciale
Esse mei Felicis opus res publica monstrat?
Pluribus haec etenim causa est curata patronis
Vt Romana salus et publica uita maneret;
Hic Petrus, hic Paulus proceres, hic martyres omnes
Quos simul innumeros magnae tenet ambitus Vrbis
Quos que per innumeras diffuso limite gentes
Intra Romuleos ueneratur Eclesia fines:
Sollicitas simul impenso duxere precatu
Excubias; Felix meus his uelut unus eorum
In precibus pars magna fuit, sed summa petiti
Muneris ad cunctos, nulli priuata, refertur.
Ergo pedem referam: sat enim mihi pauca locutum
Vnde nihil proprium meritis Felicis adesset;
Nec reticere tamen potui, quia portio laudis
Hinc quoque Felici suberat quod, summa Potestas
Rex que potens regum, Christus Deus omnibus una
Annuerat sanctis, quibus in grege supplice mixtum
Felicem parili audiuit pietate benignus;
Parcam igitur propriis adiungere publica donis
Munera, priuatos que canam Felicis honores,
Quae que suis proprie gerit hic in sedibus edam.
Vnde igitur faciam texendi carminis orsum?
Quae bona Felicis referam? quae multa per omnes
Passim agit expediam magis, anne domestica dicam
Munera, quorum ego sum specialis debitor illi?
Haec potius repetam mihi quae collata meis que
Sat memini; et quia praeteritis magis illa libellis
Dicta mihi quae partim aliis permixta que nobis
Praestitit, ex his nunc opibus quas largiter in nos
Contulit hunc animo texam gratante libellum;
Et contra solitum uario modulamine morem,
Sicut et ipse mihi uarias parit omnibus annis
Materias, mutabo modos serie que sub una
Non una sub lege dati pede carminis ibo;
Nam quasi fecundo sancti Felicis in agro
Emersere noui flores, duo germina Christi,
Turcius ore pio, florente Suerius aeuo,
Et pariter sanctae matres similes que puellae:
Alfia, qualis erat soror illa Philemonis olim
Nobilis in titulo quam signat epistola Pauli,
Et simul Eunomia, aeternis iam pacta uirago
In caelo thalamis; quam matris ab ubere raptam
Festino placitam sibi Christus amore dicauit
Vnguento que sui perfudit nominis, unde,
Tincta comas animae et mentis caput uncta pudicum,
Spirat, io! sacros Sponsi caelestis odores;
Haec Melani soror est simul et quasi filia, cuius
Haeret ouans lateri, germanum nancta magistrum;
Quae simul astrictae diuinis dotibus ambae
Virtutum uarias ut uiua monilia gemmas
Mentibus excultis specioso pectore gestant.
Has procerum numerosa cohors et concolor uno
Vellere uirgineae sequitur sacra turba cateruae;
Eunomiam hinc, Melani ductam sub principe uoce,
Formantem modulis psalmorum uasa modestis
Auscultat gaudens dilecto Christus in agno,
Quod modulante Deo benedictas paruula princeps
Sanctorum comites casto regat ore choreas.
Haec igitur mihimet meditanti congrua suasit
Gratia multimodis illuso carmine metris
Distinctum uariis imitari floribus hortum,
Sicut Felicis gremium florere repletum
Lumine diuerso quasi rus admiror opimum,
Hospitibus multis in eum Christo duce missis
Felici que patri denso simul agmine natis
Pignoribus, subito ut totis habitacula cellis
Per fines creuisse suos et sobria castos
Tecta sonare modos tandem sibi uocibus aptis
Gaudeat, hospitiis que suis et corpore et ore
Ipse sinu pleno dignos miretur alumnos
Et uirtute pares animas in dispare sexu:
Sicut oliuarum fecundo in colle nouellas
Laetatur senior diuino a semine Christi
Plantatos cernens inter sua rura colonos.
  Floreat ergo nouo mihi carminis area prato,
Laudibus et Domini, qui Conditor oris et artis
Omnimodae est, uario famulans pede musica currat!
Iam que intertextis elegus succedat iambis:
Sit caput herous fundamentum que libello.
Castis agendus gaudiis et hostiis
Dies refulsit laude Felicis sacer;
Quod "laude" dixi, "morte" dictum discite,
Quia mors piorum iure "laus" uocabitur,
Pretiosa Domino quae Deo rependitur.
Vnde et Propheta dicit in uerbo Dei
Vitae probatae in exitu laudem dari,
Et ante mortem praedicandum neminem
Salomonis ore sermo diuinus docet,
Laudanda quamuis quidam in hac uita gerant
Nec possit alibi quam sub isto saeculo
Laus praeparari quae canenda in exitu est;
Sed credo quoniam tota res uitae istius
Fluitans et anceps lubrico pendet statu,
Breui que nostram uertit aetatem rota,
Surgens cadens que per salebrosas uias
Quibus huius aeui cursus explicandus est;
Iccirco nos magistra Prouidentia
Monet ante finem nec sibi nec alteri
Debere quemquam plaudere et confidere,
Quamuis honesta rectus incedat uia,
Tamen timere semper offensam pedis
Donec, peractis usque metam cursibus,
Palmam petitae comprehendat gloriae.
Quare beatos martyras, quos extulit
Perfecta uirtus in coronam caelitem,
Iustis honoris debiti praeconiis
Celebramus, omnes nos eorum posteri
Confessione christiani nominis,
Quibus profuso sanguine ob sanctam fidem
Proseminarunt frugis aeternae bonum,
Vt si ambulemus martyrum uestigiis
Paribus parentum perfruamur praemiis.
Hinc ergo sanctis siue confessoribus
Seu consecratis passione testibus
Dies sacratos in quibus, functi diem
Mortalis aeui morte uitali suum,
De labe mundi transierunt ad Deum,
Populi fideles gaudiis sollemnibus
Honore Christi gratulantes excolunt,
Vt iste sancti pace Felicis dies
Quo clausit olim corporis uitam senex:
Confessionis ante functus proelia
Sed incruento consecratus exitu,
Post bella uictor, pacis assumptus die,
Vocante Christo liquit exsultans humum
Et in supernas transitum fecit domos,
Non defraudatus a corona martyris
Quia passionis mente uotum gesserat,
Nam saepe agonem miles intrauit potens
Victo que semper hoste confessor redit;
Sed, praeparata mente contentus, Deus
Seruauit illum, non coronam martyris
Negans sed addens et coronam antistitis,
Vt incruento palmam adeptus proelio
Et proeliati possideret praemium
Confessionis purpurante laurea,
Vitta que pacis in sacerdotis stola
Redimitus idem bis coronatus foret
Confessor atque presbyter Felix Dei.
Hic ergo uotis quem recolimus annuis
Non est agonis sed sepulturae dies,
Quo, separata ab inuicem substantia,
Anima euolauit ad Deum, in terram caro
Reuersa tumulo conquieuit abdita;
Et merito sanctis iste natalis dies
Notatur, in quo lege functi carnea
Mortalitatis exuuntur uinculis
Et in superna regna nascuntur Deo
Se cum que laetam spem resurgendi ferunt.
Ego semper istum sic honoraui diem
Magis hunc putarem ut esse natalem mihi
Quam quo fuissem natus in cassum die;
Lugendus etenim est ille dignius mihi
Dies in istud quo creatus saeculum
Peccator utero peccatricis excidi,
Conceptus atris ex iniquitatibus
Vt iam nocentem pareret me mater mea;
Maledictus ergo sit dies quo sum miser
Ad iniquitates ex iniquis editus,
Benedictus iste sit natalis et mihi
Quo mihi patronus natus in caelestibus
Felix ad illam exortus est potentiam
Qua me ualeret faece purgatum mea
Laxare uinclis et redemptum absoluere
De luctuosa morte natalis mei!
Eadem recurrit semper haec cunctis dies
Acto per orbem circulis anno suis,
Verum, quotannis innouante gratia
Diuersitatem munerum quae dat suo
Christus sodali donet ut Felix mihi,
Mutatur, et non ipsa quae cunctis uenit,
Varias meorum carminum causas ferens.
Videamus ergo quid mihi hoc anno nouum
Attulerit unde uocibus uernem nouis;
Non ibo longe nec procul sumam mihi
Praeterita tempore aut locis absentia:
Assunt, tenentur ipsa dona comminus;
Videtis omnes munera hoc anno data
Nobis in uno iuncta Felicis sinu,
Mancipia Christi, nobiles terrae prius
Nunc uero caelo destinatos incolas,
Quos Christus, ipse qui crearat diuites
Hoc pauperauit saeculo in regnum ut suum
Terreni honoris arce deiectos uehat:
Apronianum, Turciae gentis decus,
Aetate puerum, sensibus canis senem,
Veteri togarum nobilem prosapia
Sed clariorem christiano nomine,
Qui, mixta ueteris et noui ortus gloria,
Vetus est senator curiae, Christo nouus.
  Huic propinquat socius aequali iugo:
Aeuo minore Pinianus, par fide,
Et ipse prisco sanguine illustris puer,
In principe Vrbe consulis primi genus;
Valerius ille, consulari stemmate
Primus Latinis nomen in fastis tenens,
Quem Roma pulsis regibus Bruto addidit,
Valeri modo huius christiani consulis
Longe retrorsum generis auctor ultimus.
O uena felix! Ille, gentili licet
Errore functus, hoc suae stirpis bonum
Non capiat atro mersus inferni lacu!
Sed nos, fideli contuentes lumine
Retroacta uel praesentia humani status,
Miramur opera Conditoris ardui,
Et praeparatos a uetustis saeculis
Successionum mysticarum lineis
Pios stupemus impiorum filios;
Tamen in tenebris impiarum mentium
Lucis uidemus emicasse semina
In tempore ipso noctis antiquae sitis,
Quibus probata, quamlibet gentilibus,
Mens et uoluntas lege naturae fuit.
Hinc in quibusdam nunc eorum posteris
Veterum subinde uena respondet patrum,
Vt ille quondam Piniani nunc mei
Auctor supremus, in libertatem suis
Post regna dura uindicandis ciuibus
Lectus, nepotis huius ortum praetulit,
Qui, mente auita persequens superbiam
Potiore causa, seruitutem depulit
A semetipso, corporis uictor sui;
Pulso que regno Diaboli e membris suis
Iam spiritali pace peccati iugum
Fidelis animae casta libertas terit;
Et in hoc parentis aliquid illius refert
Puer iste Christi consulatum militans,
Quod liberandis consulens munus pium
Redemptionis opere dispensat Deo,
Prisci parentis aemulator hactenus
Quod seruitute liberat domesticos
Vt ille ciues; sed quod ille gesserat
In Vrbe et una et paruula primis adhuc
Romae sub annis, hic modo in multis agit
Diuerso in orbe constitutis urbibus,
Passim benignus et suis et exteris;
Nam liberorum plurimis ceruicibus
Seruile sanctis opibus expellit iugum,
Quos aere uinctos in tenebris carceris
Absoluit auro de catena fenoris.
Hos ergo Felix in suo sinu abditos
Mandante Christo condidit tectis suis,
Me cum que sumpsit sempiternos hospites:
His nunc utrimque laetus adiutoribus
Trium sub una uoce uotum dedico,
Vno loquente spirituum affectu trium:
Magnificate Deum me cum et sapienter honestis,
Vnanimes pueri, psallite carminibus!
Vt decachorda sonant pulsis psalteria neruis
Et paribus coeunt dissona fila modis,
Sic pia compagis nostrae testudo resultet
Tamquam uno triplex lingua sonet labio;
Tres etenim numero sumus, idem mentibus unum,
Et plures coeunt in tribus his animae,
Quarum caelestis liber indita nomina seruat:
Prima chori Albina est compare Therasia,
Iungitur hoc germana iugo ut sit tertia princeps
Agminis hymnisonis mater Auita choris;
Matribus his duo sunt tribus uno pignora sexu,
Flos geminus, Melani germen et Eunomia;
Haec eadem et nobis maribus sunt pignora, nam quos
Discernit sexus consociat pietas.
Cum patre Paulino pater aeque Turcius iste est;
Sed me aetas, suboles hunc facit esse patrem:
Diuerso ex aeuo sociamur nomen in unum,
Et non ambo senes sed tamen ambo patres.
Ergo cohors haec tota simul: tria nomina matres,
Quattuor in natis, in patribus duo sunt;
Nam puer hinc, Melani coniunx in corpore Christi,
Cui Deus a pinu nomen habere dedit,
Natus ut, aeternae uitae puer, arbore ab illa
Susciperet nomen quae sine fine uiret:
Pinus enim, semper florente cacumine perstans,
Semper amans celsis alta comare iugis,
Non mutat speciem cum tempore, namque sub aestu
Et niue par sibimet stat uiridante coma,
Fertilis et fructu ualidae nucis intus ad escam,
Lac tenerum crispo tegmine mater habet,
Pinguis odoratum desudat taeda liquorem
Vt nec in ipso arbor robore sit sterilis.
Haec igitur typus est aeterni corporis arbor,
Pulchra, ferax, uiuax, ardua, odora, uirens;
Istius instar erit Domino puer iste beatus
Arboris ut maneat gratia perpes ei,
Iam que Deo plantatus agit sancto que profectu
Fructiferum attollit pinus ut alta caput.
Eminet hic proprio mihi filius in grege primus,
Ast aliud mihi par lumen in Asterio est,
Quem simul unanimes uera pietate parentes
Infantem Christo constituere sacrum,
Vt, tamquam Samuel primis signatus ab annis,
Cresceret in sanctis uotus alente Deo;
Prima puer Christi sub nomine murmura soluit,
Et Domini nomen prima loquela fuit;
Iam que parente Deo regnis caelestibus ortus,
Sidereo pariter nomine et ore micat;
Hunc puerum et fratrem fecit pia Gratia patri,
Nam pariter sancto flumine sunt geniti:
Quos natura gradu diuiserat, hos Deus almo
Munere germanos in sua regna uehet.
  Ergo nouem cuncti, socia cum prole parentes,
Pectore concordi simus ut una chelys,
Omnes ex nobis citharam faciamus in unum
Carmen diuersis compositam fidibus;
Aemilius ueniat decimus: tunc denique pleno
Concinet in nobis mystica lex numero;
Hoc etenim numero capitum in testudine pacis
Viua salutiferum chorda loquetur opus;
Huic citharae plectrum Felix erit, hoc decachordam
Christus ouans citharam pectine percutiet;
Quae cithara in nobis Christo modulante sonabit
Plena perfectis sensibus harmonia
Si pax nostra Deo sit totis consona fibris,
Simus ut uniti corpore, mente, fide:
Talis enim citharam sanctis homo legibus implet,
Omnibus ad uitam compositus numeris,
Cuius uita sacrae concordat ad omnia legi;
Omnis enim irrupto stamine chorda canet.
Nunc ad te, uenerande parens, aeterne patrone,
Susceptor meus et Christo, carissime Felix,
Gratificas uerso referam sermone loquelas;
Multa mihi uariis tribuisti munera donis,
Omnia praesentis uitae rem spem que futurae
Quae pariunt tibi me memini debere, cui me
Mancipium primis donauit Christus ab annis.
Si mihi flumineis facundia curreret undis
Ora que mille forent centenis persona linguis,
Forte nec his opibus collato fonte refertus
Omnia Felicis percurrere munera possem,
Quanta suo Dominus donauit Christus amico
Et mihi confessor famulo transfudit alumno.
Quae quibus anteferam? donis diuersa sed aequis
Grandia ponderibus concurrunt multa, nec ex his
Quid potius memorare legam discernere possim:
Iudicii facilis discrimen copia turbat.
Si prima repetens ab origine cuncta reuoluam
Quae pietate pari uario mihi praestitit aeuo,
Ante queam capitis proprii numerare capillos
Quam tua circa me, Felix bone, dona referre!
Tu mihi caelestum, si possem attingere, rerum
Prima salutiferis iecisti semina causis;
Nam, puer Occiduis Gallorum aduectus ab oris,
Vt primum tetigi trepido tua limina gressu,
Admiranda uidens operum documenta sacrorum
Pro foribus feruere tuis ubi corpore humato
Clauderis et meritis late diffunderis altis,
Toto corde fidem diuini nominis hausi,
In que tuo gaudens adamaui lumine Christum.
Te duce fascigerum gessi primaeuus honorem,
Te que meam moderante manum, seruante salutem,
Purus ab humani capitis discrimine mansi.
Tunc etiam primae <libans> libamina barbae
Ante tuum solium quasi te carpente totondi;
Iam tunc praemisso per honorem pignore sedis
Campanis metanda locis habitacula fixi,
Te fundante tui uentura cubilia serui,
Cum tacita inspirans curam mihi mente iuberes
Muniri sterni que uiam ad tua tecta ferentem,
Attiguum que tuis longo consurgere tractu
Culminibus tegimen sub quo prior usus egentum
Incoluit; post haec geminato tegmine creuit
Structa domus nostris quae nunc manet hospita cellis;
Subdita pauperibus famulatur porticus aegris
Quae nos impositis super addita tecta colentes
Sustinet hospitiis, inopum que salubria praestat
Vulneribus nostris consortia sede sub una,
Commoda praestemus nobis ut amica uicissim,
Fundamenta illi confirment nostra precantes,
Nos fraterna inopum foueamus corpora tecto.
Ergo ubi bis terno dicionis fasce leuatus
Deposui nulla maculatam caede securim,
Te reuocante soli quondam genitalis ad oram
Sollicitae matri sum redditus; inde propinquos
Trans iuga Pyrenes adii peregrinus Hiberos:
Illic me thalamis humana lege iugari
Passus es, ut uitam commercarere duorum
Per que iugum carnis duplicata salus animarum
Dilatam unius posset pensare salutem.
Ex illo, quamuis alio mihi tramite uita
Curreret atque alio colerem procul absitus orbe,
Qua maris Oceani circumsona tunditur aestu
Gallia, mente tamen numquam diuulsus ab ista
Sede fui semper que sinu Felicis inhaesi,
In que uicem sensi Felicem assistere nostris
Rebus in omne bonum per cuncta domi que foris que
Conficienda: mihi res et defensio rerum
Vnus erat Felix placato numine Christi,
Semper et auertens aduersa et prospera praestans.
  Tu, Felix, semper felix mihi, ne miser essem
Perpetua pater et custos pietate fuisti,
Cum que laborarem germani sanguine caesi
Et consanguineum pareret fraterna periclum
Causa mihi censum que meum iam sector adisset,
Tu mea colla, pater, gladio, patrimonia fisco
Eximis et Christo Domino mea me que reseruas;
Nam quo consilio rebus capiti que meo tunc
Christus opem tulerit, Felicis cura potenter
Affuerit, docuit rerum post exitus ingens
Quo mutata meae sors et sententia uitae:
Abiurante fide mundum patriam que domum que,
Prodita diuersis egi commercia terris
Portandam que crucem distractis omnibus emi.
Res igitur terrae regni caelestis emit spem:
Spes etenim fidei carnis re fortior; haec spes
Perpetuam quae nixa Deo est rem parturit, at res
Carnea caelestem perimit spem, quae tamen et rem,
Si superet uincente fide, non protinus aufert,
Sed bene mutatam diuino iure reformat,
De fragili aeternam referens terris que remotam
In caelis statuens, ubi fidus credita custos
Christus habet; neque tantum isto quo sumpserit istinc
Depositum numero seruat, sed multiplicato
His qui crediderint commissa talenta rependet
Fenore, se que ipsum credentibus efficiet rem.
Et quae res hac re poterit pretiosior esse?
Si totus mundus mihi res priuata fuisset,
Num potior Domino foret haec possessio Christo?
Et quis me tantae uel spe modo possessorem
Praestitit esse rei? quis me rem compulit istam
Spernere pro Christo ut Christum mihi uerteret in rem?
Quis nisi tu, semper mea magna potentia, Felix,
Peccatis inimice meis et amice saluti?
Tu mihi mutasti patriam meliore paratu,
Te mihi pro patria reddens; tu carnea nobis
Vincula rupisti, tu nos de labe caduci
Sanguinis exemptos terrae genitalis ab ora
Ad genus emigrare tuum, et caelestia magnis
Fecisti spirare animis; tu, stemmata nostra
Mutans de proauis mortalibus inter amicos
Caelestis Domini, libro signata perenni
Nomina translato mortalis originis ortu
Deleri facies morti, transcripta saluti;
Quid simile his habui cum dicerer esse senator
Qualia nunc istic habeo cum dicor egenus?
Ecce mihi per tot benedicti martyris aulas
Et spatiis amplas et culminibus sublimes,
Et recauis alte laquearibus ambitiosas
Irriguas et aquis et porticibus redimitas
Vndique, ubique simul quodcumque per ista beati
Nomine Felicis colitur, celebratur, habetur,
Omnibus in spatiis domus est mea; nec locus ullus
Aedibus illius coniunctus et insitus exstat
Qui mihi non quasi res pateat mea. Sed quid in isto
Munere me iactem si rem Felicis amati,
Visibili lapidum tecto uernaculus hospes,
Possideam? Quanto plus est mihi quod mihi Felix,
Ipse Dei dono domus, est in quo mea uiuam
Vita domum nullis lapsuram possidet annis!
Nam quod Felicis domus et mea sit domus ipso
Permittente sui licitas mihi iuris habenas,
His etiam probat officiis audacia nostra
Hospita quod socios in tecta recepimus; et nunc
Omnes iure pari Felicis iura tenemus,
Felicis que patris gremio coniuncta fouemur
Pignora quae nostis, quos cernitis et modo in ipsis
Felicis tectis me cum metata tuentes
Hospitia, oblitos ueterum praecelsa domorum
Culmina, et angustis uicino martyre cellis
Tutius in paruo spreta ambitione manentes.
Christus enim iuxta est modicis, auertitur altis,
Pauperis et tuguri magis arta tigilla frequentat
Quam praecelsa superborum fastigia regum.
  Ergo ut componam quae nunc colo tecta relictis
Culminibus, quae nunc habeo aut habuisse recordor
Si placet arbitriis sibimet componite iustis:
Quae tam pulchra domus, quis ager tam fertilis umquam
In re mortali fuerit mihi quam modo in ista
Pauperie tribuit Christus, per quem mihi abundat,
Diues inexhausto reditu possessio, Felix?
Vt uero ex ueteri relegam mea praedia censu,
Quicquid erat magnum quondam mihi qualibet in re
Terra erat et uacuae species uentosa figurae;
Siue aurum gemmae ue forent, erat illa supellex
Vile bonis pretium, pretiosum uirus auaris;
At modo, cassus opum nec opum sed uerius expers
Damnosorum onerum, secura liber habendi
Paupertate fruor, nec habent inimica sequentum
Vincula quo teneant nudum: facili leuis exit
Corpore quem nullis suffocat amoribus illex
Per uarias species mundi fallentis imago.
O ueneranda mihi et toto pretiosior orbe
Pauperies! Christi thesauro caelite ditas
Quos spolias opibus, terrae quasi rudere purgas,
Destruis in nobis terrena, aeterna uicissim
Construis, in pretium uitae dispendia terrae
Vertis iure nouo, uersa uice detrimenti
Atque lucri, ut nobis seruata pecunia damnum,
Non seruata lucrum faciat. Sed more sinistro
Fusa eadem damno est; nec enim nisi nomine Christi
Praecepto que Dei cuiquam sua fundere prodest,
Nam uere pereunt uitiis impensa profanis;
Luxus et ambitio in magno discrimine morbi
Crimen auaritiae pensant, quia par in utroque
Causa subest mortis, quam sic maculosa libido
Perficit ut rerum mundi malesuada cupido.
His me diuitiis inopem cupis, optime Felix,
Vt facias uitae locupletem, et paupere cultu
Exsortem reddas mortis sine fine luendae
Diuitibus mundi, quibus auri letifer usus
Parturit aeternos sociis cum uermibus ignes.
Non solis tibi nos iunctos uis degere tectis,
Quos et in aeternae tibimet consortia uitae
Enutrire paras et ad illam ducere formam
Quam tu, sub Domini perfectus imagine Christi,
Gessisti in terris, homo quondam ex diuite pauper;
Nam cui paupertas tua, quam pro nomine sancto
Proscriptis opibus gaudens confessor adisti,
Ignorata latet, qua praeditus usque senectam
Conducto felix coluisti semper in horto?
Propterea similes tibi niteris efficere omnes
Paupertate pia quos suscipis hospite tecto:
Dissimilis nec enim tibi posset forma coire.
Quantum etenim discors agno lupus et tenebris lux,
Tantum dispescunt uia diuitis et uia Christi,
Nam uia lata patet quae prono lubrica cliuo
Vergit in infernum, quae dites urget auaros
Molibus impulsos propriis in tartara ferri;
At uia quae Christi est, quae confessoribus almis
Martyribus que patet, paucis iter ardua pandit:
Non capit ergo uia haec farsos, excludit onustos;
Propterea famulum sectatorem que beati
Martyris astringi decet, exutum que molestis
Compedibus tenuem de paupertate salubri
Atque leuem fieri, ut portam penetrare per artam
Possit et excelsum Domini conscendere montem.
Sed quid ego, imprudens discernere pondera rerum,
Pro magnis haec pono tuis, pater optime, donis?
Quamlibet haec quoque sint mihi grandia, parua tamen sunt
Si potiora loquar; quota portio namque tuorum est
Erga nos operum reputatio muneris huius
Quod terram hospitio dederis habitanda que tecta
Condere praestiteris, cum tu, pater, et tua nobis
Viscera praebueris? Nam quid nisi uiscera nobis
Intima prompsisti, quibus interiora sepulcri,
Sancte, tui excitis ab operto puluere causis
Pandere dignatus, speciali nos tibi amore
Insertos tanto uoluisti prodere signo
Vt tacitam et fixam per tot retro saecula sedem
Corporis, alme, tui subito exsistente fauilla
Pulueris in nostro reserari tempore uelles?
Ergo illas Felicis opes in laudibus eius
Transcursu properante legam quasi dona minora
Multa suo nobis quae iam gremio susceptis
Sedulus attribuit, neque parcet prodigus in nos
Iugiter affluere innumeris ope diuite donis;
Non ea suppeditans tantum quibus indiget usus
Corporis, illa etiam quibus et nunc gratia laudis
Quaeritur et post <hinc> retinetur nomen honoris
Addidit, ut tantis numquam retro comptas saeclis
Nostro opere exstructas accrescere uel renouari
Porticibus domibus que suas permitteret aulas:
Ille etiam proprii nobis secreta sepulcri
Sancta reuelauit... Paucis uenerabile munus
Eloquar, ut magnae pietatis luceat instar
Qua nos indignos tanto dignatus amore est
Vt, prope ad arcanum permittens nostra uerendum
Lumina, ceu propriis sua proderet ossa medullis;
Ergo suam toto uobis loquar ordine causam
Qua tribuit uicina suis nos cernere membris
Atque ipsam positi contingere corporis arcam.
Nota loci facies cunctis manet, ut super ipsum
Martyris abstrusi solium, claudente sepulcri
Cancello latus, in medio sit pagina quaedam
Marmoris affixo argenti uestita metallo;
Ista superficies tabulae gemino patet ore,
Praebens infusis subiecta foramina nardis,
Quae, cineris sancti ueniens a sede reposta,
Sanctificat medicans arcana spiritus aura.
Haec subito infusos solito sibi more liquores
Vascula de tumulo terra subeunte biberant;
Qui que loco dederant nardum exhaurire parantes
Vt sibi iam ferrent, mira nouitate, repletis
Pro nardo scyphulis cumulum erumpentis harenae
Inueniunt; pauidi que manus cum puluere multo
Faucibus a tumuli retrahunt. Noua res mouet omnes,
Et studium accendit subiti disquirere causam
Prodigii; placet ergo diem condicere certam
Scrutari et penitam summoto marmore sedem;
Hoc etenim, fateor, nimis anxia cura timebat:
Ecquid forte pio de corpore puluis haberet
Quem manus e tumulo per aperta foramina promptum
Hauserat, et uaria concretum sorde ferebat
Cum ossiculis simul et testis cum rudere mixtis?
Inde metus hominum per mutua uerba patebant
Ne fortasse sacram sancta de carne fauillam
Bestiola occultis aliqua interclusa cauernis
Altius exspueret: sicut deserta per agros
Monstra solent terram rostris fodere intus acutis
Et foueas circum cumulos effundere nigros,
Sic et ab interno sancti Felicis operto;
Quo magis hoc mirum foret, interualla dierum
Fecit congestae miranda eruptio terrae.
Ergo die placita, multis opus utile rebus
Arripitur, cunctos transmittit episcopus ad nos
Presbyteros; his fabra manus spectantibus instat
Iussa sacerdotum facere; et primus labor illis
Cancellos remouere loco cura que sequenti
Haerentes tabulas resolutis tollere clauis.
Verum ubi depressam sub tegmine marmoris arcam
Vidimus irrupta solii compage manentem,
Tunc secura fides dubio de corde periclum
Erroris pepulit, cum tactu oculo que probaret,
Incolumi solio nusquam rimante sepulcro,
Vndique uallatum ualido munimine corpus
Martyris emeriti nullis patuisse piaclis
Et dignum retinere suae pia carnis honorem
Ossa, quibus sanctus numquam desistit adesse
Spiritus, unde piis stat gratia uiua sepulcris
Quae probat in Christo functos sine morte sepultos
Ad tempus placido sopiri corpora somno.
Ergo reformato Felicis honore sepulcri,
Omnia sollicite munita relinquimus, ut iam
Vsque diem Domini, quo debita principe Christo
Excitis pariter radiabit gloria sanctis,
Inconcussa suo requiescant ossa cubili,
Quae que animam sanctam manet in regione superna
Pax eadem in terra teneat uenerabile corpus.
  Quid superest quod adhuc referam, quasi uero uel ipsa
Quae cecini digne ediderim uel cuncta profusi
Munera retulerim pleno sermone patroni?
Multa latent numero, memori tamen omnia nobis
Pectore fixa sedent, et plurima iam memorata,
Plura etiam memoranda manent; sed maxima multis
Excerpenda monet moderandi regula libri.
(672) Dicam igitur modo munus aquae; da nunc mihi, Felix,
A Domino exorans Verbo mihi currere uerbum
Tam facili eloquio quam largo flumine fontes
(675) In tua uestibula atque domos manare dedisti;
Omnia quae nobis te suffragante benignus
Contribuit Dominus tali decorauit et auxit
Munere quo fontes sitientibus intulit aruis;
Illa pio rursus Petra Christus ab ubere fluxit,
Antiquae referens donum pietatis ut amne
Insolito siccam prius irroraret harenam,
Et terram sine aqua subitis manare fluentis
Efficeret sanctas que sui Felicis in aulas,
Hospitibus populis diuersa gente coactis,
Per puteos simul atque lacus conchas que capaces
Largiter infusis noua currere pocula riuis.
Quis mea te, Fons summe, daret deserta rigare
Pumiceum que mei cordis perrumpere saxum,
Te que petra fundare domum et de te bibere undam
Quae pareret uiuam mihi sicco in pectore uenam
Aeternum salientis aquae? Sed et haec mihi gutta
Eloquii tenuis quo te loquar inde profecto
Ducitur unde etiam fluuiis exundat origo,
Nam quis uel modico te, summa Potentia summi,
Christe, Patris fari sine te queat? Ipse tuus te
Spiritus inspirat dici, quo lumine lumen
Et Patris et Nati par cernimus, ut Duce sancto
Aspirante Deum fateamur cum Patre Christum;
Sed quia dum uiui Fontis gero nomen in ore
Gutta meum stillauit in os de flumine Verbi,
Forte aliquem referent ex hinc mea labra saporem,
Et nunc munus aquae non siccis faucibus arens
Lingua sed uberius uelut humectata loquetur.
(704 | 650) Omnibus instructis operum quae multa uidentur
Diuersis exstare modis, excelsa per aulas
Et per uestibula extentis circumdata late
Porticibus, solum simul omnia munus aquarum
Tecta uidebantur maestis orare colonis.
Ipsum etiam, fateor, querula iam uoce solebam
Felicem incusare meum, quasi segniter istis
Instaret uotis, quod aquae consortia nobis
Tam longum socia pateretur ab urbe negari;
Verum inconsulta properantes mente trahebat,
Consilio potiore moras in tempora nectens
Congrua; sic etenim iusta ratione petebat
Ordo operum prior esset ut his perfectio coeptis
Quae circa sanctas uenerandi martyris aulas
Sedula multiiugo molimine cura parabat,
Cum que manum summam factis diuina dedisset
Gratia, tunc pleno finitis ordine uotis
Condita perductos riuaret in atria fontes;
Denique ut impleto stetit hic opus omne paratu,
Non est tracta diu nostri sitis arida uoti,
(670) Mox que uolente Deo populi prius aspera corda
Consensum facilem procliui corde dederunt.
(705) Postulat iste locus deuotae nomen Abellae
Indere uersiculis, nam digna uidetur honore
Nominis huius ut in laudem Felicis et ipsa
Laudetur, quia pro Felicis honore laborem
Sponte sibi sumpsit, quo desudare sub aestu
Rupibus abruptis requiem pretium que putauit.
Parua quidem haec muris, sed sancto magna feretur
Vrbs opere hoc; nostrae hinc sex milibus absita Nolae,
Altiiugos montes interiacet, ex quibus ortas
Comminus haurit aquas et in unam suscipit arcam,
Vnde per insertos calices sibi prima fluentum
Vindicat et reliquo Nolanam proluit urbem
Flumine, multa rigans et in agris praedia passim.
  Sed redeam ad grates operis pro munere habendas;
Namque operas ad aquaeductum, quem longa uetustas
Ruperat, ad sua uasa iterum formas que uocandum
Praebuit ubertim gratis operis. Locus altis
Insertus scopulis nullo neque calle uiarum
Iumentis etiam penetrabilis esse negabat,
Vnde etiam mercede manum reperire paratam
Difficile immensi faciebant ardua montis;
Quo maior mercedis honor locupletat Abellam,
Quod prompte famulata sacro Felicis honori
Effusa pietate manus impendit inemptas:
Cernere erat trepidas tota de plebe cateruas
Ordinis et populi simul una mente coactas,
Mane nouo excitos ad opus concurrere laetos,
Certatim que alacres in summa cacumina ferri,
Et, sub fasce graui cophini ceruice subacta
Caementis que simul dumosa per ardua uectis,
Sole sub ardenti crebros iterare recursus,
Et totam quam longa dies aestate moratur
Tendere ab aurora seras in uesperis horas,
Peruigiles que animis, modica uix nocte refectis
Corporibus, rursum ante diem fabrilia ad arma
Surgere, nec sentire Deo uegetante laborem.
Denique sic operis processit gratia magni
Vt tamquam ludo paucis opus omne diebus
Sumeret explicitum perfecto munere finem,
Forma que longinquis a montibus agmine farso
Qua fuerat longo prius interrupta ueterno,
Vndique fonticulis diuersa ex rupe receptis,
Collectam reuocaret aquam sitientibus olim
Vrbibus, et pleno per milia multa uiarum
Tramite formarum et nostri Felicis inundans
Tecta nouum calicem fluuio superante repleret;
Et, quod diuini documentum muneris egit,
Largior aestiuis huc mensibus unda cucurrit
Quam prius hibernis ex imbribus ire solebat.
Hinc ego te modo iure ream, mea Nola, patrono
Communi statuam, et blandae pietatis ab ira
Mente manens placida motum simulabo paternum,
Filiolam increpitans ueteris sub uoce querelae:
Nam mihi, Nola, tui consortia iusta petenti
Fontis, eo turbata metu quasi dura negabas
Hospitium communis aquae, diuina que iura
Respicere oblita, humanis mea uota putabas
Iuribus et mihi te, Felicem oblita, daturam
Credebas, ac, si tribuisses, mox tibi siccam
Subducto patriam potu fore maesta gemebas.
Id que etiam moto clamabas saepe tumultu,
Nescia diuinis opibus promptos fore fontes
Sicut et experta es; nam mox Deus ipse Creator
Arguit ignauas Christo diffidere mentes,
Cum tibi post placitam pacem iam fluminis usum
Felici consorte pie partita teneres,
Quo magis exsuperante tuam bonitate querelam
Argueret; non, ut metuebas, ille sitire
Diuisa te fecit aqua, sed, ut Auctor et Altor
Rerum hominum que simul, qui condidit omnia Verbo
Ostendit tibi rem esse suam quam tu eius amico
Vt propriam, Domino rerum diffisa, negabas.
Vidisti certe, nam te res ipsa fateri
Compulit aeterni sublimia iura Parentis
Felicis que potens meritum, cum larga sub aestu
Proflueret, damno que pio quo martyris aulis
Tradideras partem subitos creuisse meatus,
Laxato que suis in faucibus ubere fontis
Iussa fluenta tibi cum fenore reddita multo
Moenibus influxisse tuis; et tempore in ipso
Quo totius aquae possessor egere solebas,
In multum referente Deo quod sumpserat a te
Fluxit abunda tuis aqua potibus atque lauacris.
Quodnam igitur tanto pro munere munus, Abella,
Pauper opum referam tibi? Saltem carmine nostro
Obsequium nomen que tuum dum praedico signans
Hoc pensabo tibi pretium mercedis; honore
Felicis sancti scribaris, ut addita semper
Laudibus et tanti memoreris alumna patroni,
Cuius donorum tua maxima portio facta est.
Nunc tuus iste labor, quo te Felicis adegit
Spiritus ut, tota tibi plebe uel ordine concors,
Aggredereris opus magno sudore parandum
Tempore et aestiuo durum duplicante laborem
Molis ut antiquae per iniqua cacumina formam
Praeteritis notam saeclis, iam uero sine usu
Deficientis aquae, superactis undique multa
Congerie siluis inter iuga uasta latentem,
Exuis aggeribus densis, oblita que dudum
Munia restituis, sparsas que per auia uenas
Pumiceis alte quae sorbebantur harenis
Colligis, et sua quamque sequi uestigia rursus
Cogis aquam et reducem formae matricis in oram
Inuehis, et dudum uacuos cessante meatu
Exundare facis fluctu remeante canales,
Atque diu querulam subductis fontibus urbem
Iam desperato perfundis flumine Nolam,
Quae fruitur Felicis aquis - quia copia non est
Haec ipsi sua nunc urbi quam nuper adepta est
Felicis studio modicae pro munere guttae
Ex ope diuina largis ditata fluentis.
Ergo et tu me cum paruam quasi mater Abellam,
Nola, foue, quoniam, ut cognoscis, et ipsa tuarum,
Filia cum tua sit, tamen est tibi mater aquarum,
Cuius ab indigenis tibi montibus affluit omnis
Copia, qua fueras felicibus ante superba
Et qua post studio meliore ministra fuisti.
  Gaude igitur, mea Nola, tibi et gratare profusis
Viribus, exsultans Christo qui te per amicum
Dilectum que suum Felicem finxit et auxit
Natura famulante tuum, manus alta, decorem;
Cerne tuam faciem qua nunc noua praenituisti,
Vt noscas dederis ne aliquid Felicis honori
An magis a Felice, Deo cumulata, colaris:
Asper ubi nudis arebat calculus aruis
Nunc mutata uiret madefactis gratia glaebis.
Non istos tantum fontes tibi, Nola, profudit
Felicis merito diues tibi gratia Christi;
Caelifluos etiam fontes huc ad tua duxit
Moenia, Felicis que sinu gaudente locatos
Diffluere in multas effusis amnibus urbes
Vrbe tua iussit: famulos Christi, loquor, istos,
Par illustre Deo, <par> nobile nomine Christi,
Albina cum matre tuis modo finibus ortos,
Pignora cunctorum sanctorum et gaudia caeli,
Piniadem Melani cum foedere par benedictum.
Hos Deus et natos Felicis et ubera fecit,
Vbera diuinae bonitatis proflua lacte;
Et quibus omnis inops alimenta fluentia sumit,
Omnis item diues documenta salubria sumit;
Hi sunt ecce pio Christi de flumine fontes:
Qui non uisibili per terram gurgite manant,
De uiua miserantis aqua pietatis abundant.
Hos tu, Christe, tibi praesta ubertate perenni
Scaturrire tui Felicis in ubere fontes,
Et numquam has ullo tenuari sidere uenas;
Influe pectoribus semper tibi, Christe, dicatis,
Felici que tuo de peccatoribus ipsi
Mandatis tribue ut numquam pietas tua nostris
Visceribus fontem huius opis subducat, et ipse
Fons a fonte tuo Felix nos largus inundet,
Semper ut in nobis saliat, Rex Christe, tuus fons;
Et nos, de miseris et egenis, sorte sui iam
Nominis obtineat felices uiuere Felix. 