 Lex mihi iure pio posita hunc celebrare quotannis
Eloquio famulante diem sollemne reposcit
Munus ab ore meo: Felicem dicere uersu
Laetitiam que meam modulari carmine uoto
Et magnum cari meritum cantare patroni,
Quod per iter durum qua fert uia peruia paucis,
Alta per arta petens, superas penetrauit ad arces.
Concordate meis, precor, et complaudite, fratres,
Carminibus, casto que animos effundite luxu!
Gaudia sancta decent et carmina casta fideles,
Nam cui fas hominum cui Christus amor que timor que est
Non gaudere hodie, et uacuum procedere uoti
Qua quis possit ope ingenii linguae que rei que,
Caelicolas Christo quando aggaudere ministros
Ipsa etiam festo produnt elementa colore?
Cernite laetitiam mundi in splendore diei
Elucere sacris insignibus: omnia laetus
Candor habet, siccus teneris a nubibus imber
Ponitur et niueo tellus uelatur amictu
Quae, niue tecta solum, niue siluas, culmina, colles
Compta, senis sancti canos testatur honores;
Angelica que docent et luce et pace potiri
Felicem placida clarum in regione piorum,
Lactea quae tacito labuntur uellera caelo.
Christe, Deus Felicis, ades: da nunc mihi uerbum,
Sermo Deus, da perspicuam, Sapientia, mentem!
Non opis humanae facundia dicere laudes
Posse tuas - tua namque tui sunt gloria sancti.
Cedo, alii pretiosa ferant donaria, me que
Officii sumptu superent qui pulchra tegendis
Vela ferant foribus, seu puro splendida lino
Siue coloratis textum fucata figuris;
Hi leues titulos lento poliant argento
Sancta que praefixis obducant limina lamnis;
Ast alii pictis accendant lumina ceris
Multifores que cauis lychnos laquearibus aptent,
Vt uibrent tremulas funalia pendula flammas;
Martyris hi tumulum studeant perfundere nardo
Vt medicata pio referant unguenta sepulcro.
Cedo equidem et uacuo multis potioribus auro
Quis grauis aere sinus releuatur egente repleto,
Qui locuplete manu promptaria ditia laxant
Et, uariis animam sponsantes dotibus, astant
Mente pares, ope diuersi; nec segnius illi
Fercula opima cibis, ceras, aulaea, lucernas -
Larga quidem sed muta - dicant; ego munere linguae
Nudus opum famulor, de me mea debita soluens,
Me que ipsum pro me uilis licet hostia pendo;
Nec metuam sperni, quoniam non uilia Christo
Pauperis obsequii libamina, qui duo laetus
Aera piae censum uiduae laudata recepit.
Tunc quoque multa Deo locupletes dona ferebant,
Implentes magnis aeraria sancta talentis,
Sed Christus spectator erat, qui, corda ferentum
Inspiciens, uiduae palmam dedit: illa diurni
Rem uictus, geminos quod ei substantia nummos,
Miserat in sacram, nil anxia corporis, arcam;
Propterea ex ipso uenturi Iudicis ore
Ante diem meruit facti praecerpere laudem,
Praeferri que illis quorum stipe uicerat aurum,
Munere pauper anus, sed prodiga corde fideli.
Ergo, boni fratres, quibus huc dignatio et istic
Concessus, placidis aduertite mentibus aures,
Nec qui sed de quo loquar exaudite libenter;
Despicienda quidem, tamen et miranda profabor:
Despicienda meo ingenio, miranda beati
Felicis merito, quod dicere non sine Christi
Laude licet, quia quicquid in hoc miramur ab illo est
Vnde piis uirtus et per quem uita sepultis. 
 Praeteritis cecini patriam, genus, acta libellis,
Et tota sanctum repetens ab origine duxi
Felicem, donec perfectae tempora uitae
Clauderet, et posito desertis corpore terris
Tenderet aeterni merita ad consortia regni;
Sed quia non idem tumuli qui membra piorum
Et merita occultant, animarum uita, superstes
Corporibus functis, quaesitos corpore fructus
Et post corporeos obitus non mortua sentit:
Laeta bonos, cruciata malos; quos, rursus in ipsum
Tempore uenturo corpus reuocata, remixto
Corpori communi metet indiscreta recepto.
Longa igitur mihi materies! quantum que erit aeui,
Tantum erit et uerbi super hoc quo dicere gesta
Felicis pateat, si copia tanta sit oris
Quanta operum meriti que manet; nam tempore ab illo
Quo primum ista dies Felicem fine beato
Condidit et carnem terris, animam dedit astris,
Ex illo prope cuncta dies operante uidetur
Confessore Dei; probat et sine corpore uiuum
Christus, ut ostendat maiorem in morte piorum
Virtutem quam uim in uita superesse malorum.
Ecce uides tumulum sacra martyris ossa tegentem
Et tacitum obtento seruari marmore corpus;
Nemo oculis hominum, qua corpore cernimus, exstat:
Membra latent positi, placida caro morte quiescit,
In spem non uacuam rediuiuae condita uitae;
Vnde igitur tantus circumstat limina terror?
Quis tantos agit huc populos? quaenam manus urget
Daemonas, inuitos que rapit, frustra que rebelli
Voce reclamantes compellit adusque sepulcrum
Martyris et sancto quasi fixos limite sistit?
Respicio hanc aliquando diem: quam maesta relicto
Orbe fuit! quam laeta polo cum Christus amicam
Assumens animam casto Deus hausit ab ore!
Addidit ornatum caelis, nec pignore terras
Orbauit: superi Felicis mente fruuntur,
Corpore nos, animae que potentis spiritus illic
Viuit, et hic meritum. Sed totum funeris almi
Praesentare iuuat quem Nola impendit honorem;
Namque sacerdotem sacris annis que parentem
Perdiderat, sed eum caelis habitura patronum
Vrbs deuota pium, spe solabatur amorem;
Totis ergo quibus stipatur conflua turbis
Currit in obsequium, populos effusa fideles.
Tunc dolor et pietas coeunt in pectora cunctis;
Admixta pietate fides gaudet que dolet que,
Et, licet accitum Christo super aethera tolli
Felicem credat, tanto tamen ipsa relinqui
Praeceptore dolet; quod que unum in funere sancto
Inter et exsequias restat solamen amoris,
Postquam depositum tumulandi in sede feretrum,
Certatim populus circumfusus pietatem
Vndique denseto coetu sita membra coronat;
Religiosa pie pugna exercetur amantum:
Quisque alium premere et propior consistere certat
Reliquiis, corpus que manu contingere gaudet.
Nec satis est uidisse semel, iuuat usque morari
Lumina que expositis, et, qua datur, oscula, membris
Figere; dat meritam Christo plebs consona laudem,
Molitur que sacrum solii Felicis honorem.
Qua muris regio et tectis longinqua uacabat,
Fusus ibi laeto ridebat caespite campus;
Vberius florente loco, quasi praescia iam tunc
Semper honorandi mundo uenerante sepulcri,
Gaudebat sacro benedici corpore, se que,
Veris amoena habitu, quo dignior esset humando
Martyre graminibus tellus sternebat odoris.
 Ast illum, placido scandentem celsa uolatu
Et casto assumptum de corpore, laeta piorum
Turba per aetherias susceperat obuia nubes;
Angelici que chori, septemplicis agmina caeli
Totis qua caelum patet occurrentia portis,
Regis in aspectum summi que Parentis ad ora
Sidereo uolucrem laeti uexere triumpho.
Tum niuea sacrum caput ornauere corona,
Sed tamen et roseam Pater addidit indice Christo
Purpureo que habitu niueos duplicauit amictus,
Quod meritis utrumque decus; nam lucida sumpsit
Serta quasi placido translatus in aethera leto,
Sed meruit pariter quasi caesi martyris ostrum:
Qui confessor obit tenet ergo et praemia passi
Quod prompta uirtute fuit, nec pacis honore
Ornatu que caret, quia non congressus obiuit.
Facta igitur rata iusta, pium texere sepulcro
Funus; at in sanctis diuinitus insita membris
Gratia non potuit cum carne mori que tegi que;
Ilico sed positis ex ossibus emicuit lux
Quae medicis opibus meriti dare signa potentis
Hactenus ex illo non umquam tempore parcit,
Et toto quo mundus erit fulgebit in aeuo
Lux eadem, sancti cineris per saecula custos.
Martyris haec functi uitam probat et, bona Christi
Ad tumulum Felicis agens, diffundit in omnes
Felicis late terras mirabile nomen,
Dignatam et tanto prae cunctis urbibus unam
Hospite nobilitat Nolam, quam gratia Christi
Felicis meritis ita dilatauit ut, aucta
Ciuibus ecce nouis et moenibus, hic etiam urbs sit
Pauper ubi primum tumulus quem tempore saeuo
Religio quo crimen erat minitante profano
Struxerat anguste, gladios trepida inter et ignes,
Plebs Domini, ut seris antiqua minoribus aetas
Tradidit. Ingentem paruo sub culmine lucem
Clauserat, et tanti tantum sacer angulus olim
Depositi possessor erat, qui, lucis opertae
Conscius, ut quidam fons aedibus exstitit amplis,
Et manet in mediis quasi gemma intersita tectis,
Basilicas per quinque sacri spatiosa sepulcri
Atria diffundens, quarum fastigia longe
Aspectata instar magnae dant uisibus urbis.
Quae tamen ampla licet uincuntur culmina turbis,
Quod crescente fide superundat gratia Christi,
Quae populis medico Felicem munere praestat,
Viuere qui praestans etiam post corporis aeuum
Praesidet ipse suis sacer ossibus; ossa que sancti
Corporis e tumulo, non obsita puluere mortis
Arcano aeternae sed praedita semine uitae,
Viuificum spirant animae uictricis odorem
Quo medicina potens datur exorantibus aegris.
Quanta resurgentes uirtus et gloria cinget
Coniectare licet, cum gratia tanta sepultos
Ambiat, et quanto rediuiua decore micabunt
Corpora, in obscuris cum sit lux tanta fauillis!
Quid nobis minimis horum praestare coronae
Sufficient, quorum et cineres dant commoda uiuis?
Cernere saepe iuuat uariis spectacula formis
Mira salutantum et sibi quaeque accommoda uotis
Poscentum. Videas etiam de rure colonos
Non solum gremio sua pignora ferre paterno,
Sed pecora aegra manu saepe introducere se cum
Et sancto quasi conspicuo mandare licenter,
Mox que, datam sua confisos ad uota medelam,
Experto gaudere Deo, et iam credere sana -
Et uere plerumque breui sanata sub ipso
Limine laeta suis iumenta reducere tectis;
Sed quia prolixum et uacuum percurrere cuncta
Quanta gerit Felix miracula numine Christi,
Vnum de multis opus admirabile promam
Innumeris paribus; sed ab uno pende relicta
Quae uirtus eadem gessit distantia causis.
Pandite corda, precor, breuis est iniuria uobis
Dum paucis magnum exiguus opus eloquor orsis,
Et, memores uiduae primo sermone relatae
Quam Deus e pretio mentis non munere cernens,
Antetulit multum mittentibus, omnia dantem,
Me quoque ferte leui dicentem magna relatu:
Et mea namque illis sunt aemula uerba minutis
Quis pretium pietas et uilibus aurea fecit.
Quidam homo, re tenuis, plebeius origine, cultu
Rusticus, e geminis angustam bubus alebat
Pauperiem mercede iugi, nunc subdere plaustris
Suetus eos oneri pacta regione uehendo,
Nunc operae pretium sub aratra aliena locatis
Paupertatis habens reditum. Spes anxia res que
Tota inopi par illud erat; non carior illi
Progenies aut ipse sibi, sed pignora et ipsos
Ducebat; neque cura minor saturare iuuencos
Quam dulces natos educere, parcior immo
Natis quam pecori caro: non gramine uili
Illos aut sterili palea, sed tegmine aprico
Algidus et de farre sibi natis que negato
Esuriens pascebat, egens sibi, diues in illis
Quorum fecundus labor exsaturabat egentem.
Hos igitur, tam cara suae solamina uitae,
Nocte miser quadam somno grauiore sepultus
Amisit taciti furto praedonis abactos;
Exsurgens que die reduci, de more iugandos
Infelix primo in uacuis praesepibus intus
Mox que foris frustra notis quaesiuit in agris;
Ilico sed fessus cassis erroribus ultro
Atque citro, postquam nullis uestigia signis
Certa uidet, spebus frustrata indage peremptis,
Humanam desperat opem et, pietate repletus
Aspirante Deo, depressam in pectore fracto
Erigit in caelum mentem; et mox corde refecto
Praesumente fide spem uoti compotis haurit,
Sancta que Felicis rapido petit atria cursu;
Ingressus que sacram magnis cum fletibus aulam
Sternitur ante fores, et postibus oscula figit,
Et lacrimis rigat omne solum pro limine sancto
Fusus humi, et raptos nocturna fraude iuuencos
A Felice pio uelut a custode reposcit
Increpitans, miscet que precantia uerba querelis:
"Sancte Deo Felix, inopum substantia, semper
Pro miseris felix et semper diues egenis;
Te requiem fessis Deus afflictis que leuamen,
Te posuit maestis ad saucia corda medelam;
Propterea tamquam gremio confisa paterno
In te paupertas caput acclinata recumbit.
Felix sancte, meos semper miserate labores,
Nunc, oblite mei, cur me, rogo, uel cui, nudum,
Deseris? Amisi caros, tua dona, iuuencos
Saepe tibi supplex quos commendare solebam,
Quos tua perpetuo seruabat cura fauore
Pascebat que mihi! tua nam custodia saluos
Dextra que sufficiens illos praestabat opimos
Quos misero mihi nox haec abstulit. Heu! quid agam nunc?
Quo deceptus eam? quem criminer? An tibi de te
Conquerar, immemorem que mei accusabo patronum
Qui mihi sopito tam densum irrepere somnum
Ne mea sentirem perfringere claustra latrones
Passus es, et nullo fregisti dura pauore
Pectora nec lucem tenebris furto que dedisti,
Aut ullis profugos curasti prodere signis?
Qua modo discurram? quo deferar? Omnia caecis
Structa mihi latebris nunc et mea tecta uidentur
Clausa mihi, abductis ubi desolatus alumnis
Nil habeo quod habere uelim, quod dulce uidenti,
Dulce laboranti non irrita gratia praestet,
Oblectans inopem sensu fructu que peculi. 
Hos ubi nunc quaeram miserandus ego? aut ubi, quando
Inueniam tales, aut unde parabo repertos,
Qui solos habui contentae rusticus illos
Paupertatis opes? Ipsos igitur mihi redde,
Nolo alios; nec eos ulla regione requiram:
Hic mihi debentur! Haec illos limina reddent
In quibus ipsum te supplex astringo tibi que
Haereo: cur quaeram, aut ubi, quos ignoro latrones?
  Debitor hic meus est: ipsum pro fure tenebo
Custodem. Tu, sancte, reus mihi, conscius illis,
Te teneo; tu scis ubi sint, qui lumine Christi
Cuncta et operta uides longe que absentia cernis
Et capis, includente Deo quo cuncta tenentur;
Atque ideo occulti fures quacumque latebra
Non tibi celantur nec de te euadere possunt,
Quos etiam manus una tenet, Deus unus ubique
Christus, blanda piis sed iniquis dextera uindex.
Redde igitur mihi, redde boues, et corripe fures!
Sed non quaero reos, abeant; non nescio mores,
Sancte, tuos: nescis malefacta rependere, mauis
Emendare malos uenia quam perdere poena.
Conueniat nobis igitur: sic diuide me cum
Quae tua, quae mea sunt; indemnis stet mea per te
Vtilitas, iuxta que tuas clementia partes
Vindicet aequato que tuum libramine constet
Iudicium; tibi solue reos, mihi redde iuuencos.
Ecce tenes pactum; famuli iam nulla morandi
Causa tibi: accelera tantis me soluere curis,
Nam mihi certa manet sententia cedere nusquam
Donec subuenias nec ab isto poste refigi;
Ni properas, isto deponam in limine uitam,
Nec iam repperies cui reddas sero reductos!"
Talia uoce quidem querula sed mente fideli
Plorantem toto que die sine fine precantem
Audiuit, laetus non blando supplice, martyr,
Et sua cum Domino ludens conuicia risit;
Poscentis que fide, non libertate dolentis
Motus, opem properat: paucis mora ducitur horis.
Interea labente die, iam uespere ducto,
Nec precibus dabat ille modum nec fletibus; una
Vox erat affixi foribus: "Non eruar istinc,
Hic moriar uitae nisi causam protinus istic
Accipiam!" Tandem tamen, ut iam plurima tutum
Nox secretum adytis fieri cogebat, et ille,
Temporis oblitus, damni memor, ostia prono
Ore premens toto prohibebat corpore claustra;
Sed multis frustra pulsatum uocibus aures
Aggreditur uiolenta manus, tandem que reuellit
Turba reluctantem et sancta procul exigit aula.
Pulsus ab aedituis, flet amarius, et sua lugens
Tecta petit; resonant plangore silentia noctis,
Questibus et magnis late loca sola resultant,
Donec et inuitus peruenit; et, atra silentis
Ingrediens tuguri penetralia, rursus ab ipso
Culminis introitu taciti, ut praesepia uidit
Nuda boum et nullos dare tintinnabula pulsus,
Excussa ut ceruice boum crepitare solebant
Mollius aut lentis caua linguis aera ferire,
Armentum reduces dum gutture ruminat herbas;
His grauius tamquam rescisso uulnere planctum
Integrat, et, quamquam neget aegro cura quietem,
Et uigili tamen haec dat solamenta dolori
Vt bubus stabulata suis loca corpore fuso
Pressa superiaceat; nec duro fracta cubili
Membra dolent: iuuat ipsa iniuria; nec situs horret
Sordentis stabuli, quia notum reddit odorem
Dilecti pecoris, nec foetor foetet amanti.
Si qua illi extremo tulerant uestigia gressu
Aspicit et, palpante manu calcata retractans,
Ingemit, et refricat totis iam frigida membris
Signa pedum; mentem que suam, licet eminus absit
Corpore, sacratam Felicis mittit ad aulam,
Felicem fletu, Felicem nomine clamans;
Nec desperat opem nec parcit fundere uota.
Nox medium iam uecta polum perfuderat orbem
Pace soporifera, reticebant omnia somno,
Solum illum sua peruigilem spes cura que habebat;
Ecce repente suis strepitum pro postibus audit
Et pulsas resonare fores; quo territus, amens
Exclamat, rursum sibi fures affore credens:
"Quid uacua in cassum, crudeles, ostia uultis
Frangere? Iam nullus mihi bos: quid quaeritis ultra?
Praeuenere alii: mea tantum uita superstes,
Quae sociis uestris ut praedae cassa remansit."
Dixerat haec metuens, sed nullo fine manebat
Liminibus sonitus; quo crebrescente, nec ulla
Respondente sibi pulsantum uoce, propinquat
Suspensus, cunctante gradu, et dat postibus aurem
Sollicitam, et rimis aciem per hiantia claustra
Qua tenebris albus caeli color interlucet
Inserit; explorat que diu, nec adhuc sibi credit
Quid uideat; nec enim sublustri lumine noctis
Pura fides oculis. Dubio tamen ipsa per umbras
Corpora pulsantum trepidos auferre pauores
Spem que boni coepere nouis promittere formis:
Non homines pulsare uidet, sed, quod uidet, esse
Verum non audet sibi credere. Magna profabor,
Quamquam parua Deo, miracula, cui sapit omne
Rerum animal sensu quo iusserit ipse Creator
Omnigenum pecus: ecce, gerens duce Numine mentem,
Par insigne boum non nota per auia nocte
Venerat ad notas nullis rectoribus aedes,
Sponte quasi non sponte tamen, quia Numinis actu
Ereptos potiore manu praedonibus illos
Egerat occultis Felix moderatus habenis.
Et postquam attigerant assueti culmea tecti
Culmina, gaudentes reditu expertas que timentes
Sat memori terrore manus, quasi pone timerent
Instantem sibi raptorem, quatere ostia iunctis
Frontibus, et tamquam manibus sic cornibus uti
Vt dominum excirent sonitu; sed territus ille
Rursus ut hostili circum sua claustra tumultu
Tuta etiam timuit; rursus Sapientia bruto
Aspirat pecori causam sentire morantis
Atque intellectum domini reserare timentis:
Edere mugitum de quo, formidine pulsa,
Panderet exclusis aditum securus alumnis. 
Ille, inopina uidens diuini insignia doni,
Haeret adhuc, trepidum que etiam sua gaudia turbant;
Credere non audet, metuit non credere; cernit
Comminus et caligare putat; dum respicit ad se,
Diffidit tantum sese potuisse mereri,
Sed, contra reputans a quo sperauerit, audet
Credere, cognoscens Felicis gesta patroni.
Iam que rubescebant rumpente crepuscula mane,
Noctis et extremae fuga, rarescentibus astris
Luce sub obscura uel sublucentibus umbris,
Coeperat ambiguos rerum reserare colores;
Tunc demum nota specie sibi bubus apertis
Vt primum coepere oculis clarescere saetae,
Certior exsultat; remouens et pessula claustris,
Ostia laxato stridentia cardine soluit;
Dum facit hoc, iuncti simul irrupere iuuenci
Et reserantis adhuc molimina praeuenerunt:
Dimoto faciles cesserunt obice postes,
Oblatum que sibi mox ipso in limine regem
Cognoscunt hilares laetum, lambunt que uicissim
Mulcentem, labris que manus palpantis inundant,
Atque habitum totum spumosa per oscula foedant,
Dum complectentis domini iuga cara benignum
Molliter obnixi blanda uice pectus adulant;
Illum dilecti pecoris nec cornua laedunt
Et collata quasi molles ad pectora frontes
Admouet, et manibus non aspera lingua uidetur
Quae lambens etiam siluestria pabula radit.
Sed tamen haec inter non uano corde fidelis
Rusticus officii meminit, neque curat anhelos
Ante boues stabulis inducere post que laborem
Atque famem recreare cibo, quam ducere se cum
Illuc unde suos meruit; uenit ergo, reductos
Ducens, nec tacitis celat sua gaudia uotis,
Et referens densas trahit ad sua uerba cateruas
Ingreditur que sacras cunctis mirantibus aedes;
Quos miser hesterno amissos deflerat, eosdem
Praesentes hodie ducit sancti que triumphum
Martyris ostentat populis; ducuntur et ipsi
Per medios coetus modo furum praeda iuuenci
Et modo Felicis spolium; dat euntibus ingens
Turba locum, et muto celebratur gloria Christi
In pecore. Ille autem qui tanti muneris alto
Causa fuit Domino, mediis in milibus exstans
Flens que iterum sed laetitia modo, debita sancto
Vota refert; non aere graui nec munere surdo,
Munere sed uiuo linguae mentis que profusus,
Voce pia largum testatur pauper amorem
Debitor et Christo satis isto pignore soluit,
Immaculata suae cui sufficit hostia laudis:
"Captiuos en, sancte, tuos tibi plebe sub omni
Victor ago et supplex iterum tibi mando tuendos:
Conserua reduces dignatus reddere raptos;
Sed tamen in me nunc ipsum, bone, respice, martyr;
Namque uides quod agas tibi adhuc superesse, sed in me,
Qui prope caecatis oculis tua comminus asto
Limina; nam multo mersi mea lumina fletu,
Non solum damno sed et inter gaudia plorans.
Dempsisti causam lacrimarum: tolle modo orta
Vulnera de lacrimis; miseratus, sancte, meorum
Damna boum, miserare itidem modo damna oculorum!
  Donasti reduces pecudes mihi, rursus et illis
Redde meos oculos; nam quid iuuat esse reductos,
Si languente acie praesens praesentibus absim?"
Talia praesentes populi risere querentem,
Sed procul admotae secreti martyris aures
Suscepere pias ab inepto supplice uoces,
Mox que refecta sacram senserunt lumina dextram...
Inde domum gaudens oculis bubus que receptis,
Collaudante Deum populo, remeabat, et illum
Laeta sequebatur gemini uictoria uoti. 