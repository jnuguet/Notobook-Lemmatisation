Inclyte confessor, meritis et nomine Felix,
Mens pietate potens, summi mens accola caeli
Nec minus in totis experta potentia terris,
Qui, Dominum Christum non uincta uoce professus,
Contemnendo truces meruisti euadere poenas,
Deuotam que animam tormenta per omnia Christo
Sponte tua iussus laxatis reddere membris,
Liquisti uacuos rabidis lictoribus artus:
Vectus in aetherium, sine sanguine martyr, honorem,
O pater, o domine, indignis licet annue seruis
Vt tandem, hanc fragili trahimus dum corpore uitam,
Sedibus optatis et qua requiescis in aula
Hunc liceat celebrare diem, pia reddere coram
Vota et gaudentes inter gaudere tumultus.
Sit iam, quaeso, satis meritam impietate tulisse
Hanc poenam tot iam quod te sine uiximus annis,
Sede tua procul, heu! quamuis non mente remoti;
Iam desideriis immenso tempore fessis
Consule, iam uel sero memor miserere tuorum,
Per que orbem magni qui nos procul aequore ponti
Disparat, obtritis quae nos inimica retardant
Pande uias faciles; et si properantibus ad te
Inuidus hostis obest, obiecta repagula pelle,
Fortior aduersis, et amicos prouehe cursus.
Seu placeat telluris iter, comes aggere tuto
Esto tuis; seu magna tui fiducia longo
Suadeat ire mari, da currere mollibus undis
Et famulis famulos a puppi suggere uentos,
Vt Campana simul Christo duce litora uecti
Ad tua mox alacri rapiamur culmina cursu,
In que tuo placidus nobis sit limine portus.
Illic dulce iugum, leue onus blandum que feremus
Seruitium sub te domino; etsi iustus iniquis
Non egeas seruis, tamen et patiere et amabis
Qualescumque tibi Christo donante dicatos
Et foribus seruire tuis, tua limina mane
Munditiis curare sines et nocte uicissim
Excubiis seruare piis, et munere in isto
Claudere promeritam defesso corpore uitam. 